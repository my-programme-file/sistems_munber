import tkinter as tk
from tkinter import ttk
import shelve

# ПЕРЕМЕННЫЕ
window = shelve.open("window_shelve")
if (window["color"] != ""):
    window_color = window["color"]
else:
    window_color = "Grey"
if (window["size"] != ""):
    window_size = window["size"]
else:
    window_size = "530x100"
    
# СОЗДАНИЕ НАЧАЛЬНОГО ОКНА С ТИТУЛЬНЫМ ЛИСТОМ
window_1 = tk.Tk()
window_1.title("Translation of number systems")
window_1.geometry(window_size)
window_1.config(bg=window_color)